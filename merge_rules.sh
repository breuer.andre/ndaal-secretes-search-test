#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o pipefail
set -o nounset

mydir=$(cd "$(dirname "${0}")"; pwd -P)
BASE_RULESET="${mydir}/${1:-gitleaks_base.toml}"
#PII_RULESET="${mydir}/${2:-gitleaks_pii.toml}"
OUTFILE="${mydir}/${3:-gitleaks_all.toml}"
TMP_FILE="${mydir}/tmp.toml"

rm -f "$TMP_FILE" && touch "$TMP_FILE"

echo "Start merging rules in ${mydir}"
cd "${mydir}"

merge_files=$(find "${mydir}" -type f -name 'gitleaks_*.toml' ! -path "*${BASE_RULESET}" ! -path "*${OUTFILE}")
for merge_file in $merge_files; do
    sed '1,/^# Rules.*#$/d' "$merge_file" | sed '1d' >>"$TMP_FILE"
done

cat "$BASE_RULESET" "$TMP_FILE" >"$OUTFILE"

rm -f "$TMP_FILE"
echo "Done!"
exit 0