# Ndaal Secrets Search

[[_TOC_]]

## Information

This repo contains multiple versions of rulesets for detecting secrets and
other sensitive information using GitLeaks. Currently the following datasets
are present:
- [gitleaks_base.toml](/gitleaks_base.toml) &rarr; general ruleset for
  detecting secrets
- [gitleaks_aspnet.toml](/gitleaks_aspnet.toml) &rarr; ruleset for detecting
  leaked ASP.NET machine keys [^footnote-5]
- [gitleaks_pii.toml](/gitleaks_pii.toml) &rarr; ruleset for detecting PII
  (Personal Identifiable Information)
- [gitleaks_all.toml](/gitleaks_all.toml) &rarr; combination of all previously
  mentioned rulesets

**IMPORTANT:** The PII ruleset contains some very general expressions. So for
example, the National ID (CRR) of Austria consists of a 12-digit number, but
just because a 12-digit number is found it doesn't automatically mean that it
is an Austrian national ID. So please be careful when invoking this ruleset and
always consider the findings in an context. Also note, that the PII ruleset
almost certainly creates false positives.

# FAQ - Secrets Search Based on GitLeaks

## General

**GitLeaks** [^footnote-1] is a SAST tool for **detecting** and **preventing**
hardcoded secrets like passwords, api keys, and tokens in git repos. Ndaal
created a GitLeaks ruleset which is based on the RegEx expressions found in
[different sources][sources for ruleset].
The ruleset to be used with GitLeaks is in TOML-format. However, the same
infromation is also given in JSON format.

## Installation

**GitLeaks** is based on **GoLang** [^footnote-2] and can be installed as
follows (c.f. GitLeaks installation documentation [^footnote-3] ):

> ```shell-session
> # MacOS
> brew install gitleaks
>
> # Docker (DockerHub)
> docker pull zricethezav/gitleaks:latest
> docker run -v ${path_to_host_folder_to_scan}:/path zricethezav/gitleaks:latest [COMMAND] --source="/path" [OPTIONS]
>
> # Docker (ghcr.io)
> docker pull ghcr.io/zricethezav/gitleaks:latest
> docker run -v ${path_to_host_folder_to_scan}:/path zricethezav/gitleaks:latest [COMMAND] --source="/path" [OPTIONS]
>
> # From Source
> git clone https://github.com/zricethezav/gitleaks.git
> cd gitleaks
> make build
> ```

## Usage

Once, GitLeaks is installed more information on how to run are obtained via the
command `gitleaks -h`:

> ```shell-session
> Gitleaks scans code, past or present, for secrets
>
> Usage:
> gitleaks [command]
>
> Available Commands:
> completion  generate the autocompletion script for the specified shell
> detect      detect secrets in code
> help        Help about any command
> protect     protect secrets in code
> version     display gitleaks version
>
> Flags:
> -b, --baseline-path string       path to baseline with issues that can be ignored
> -c, --config string              config file path
>                                 order of precedence:
>                                 1. --config/-c
>                                 2. env var GITLEAKS_CONFIG
>                                 3. (--source/-s)/.gitleaks.toml
>                                 If none of the three options are used, then gitleaks will use the default config
>     --exit-code int              exit code when leaks have been encountered (default 1)
> -h, --help                       help for gitleaks
> -l, --log-level string           log level (trace, debug, info, warn, error, fatal) (default "info")
>     --max-target-megabytes int   files larger than this will be skipped
>     --no-banner                  suppress banner
>     --redact                     redact secrets from logs and stdout
> -f, --report-format string       output format (json, csv, sarif) (default "json")
> -r, --report-path string         report file
> -s, --source string              path to source (default: $PWD) (default ".")
> -v, --verbose                    show verbose output from scan
>
> Use "gitleaks [command] --help" for more information about a command.
> ```

For the purpose of detection of secrets the command `detect` is used. To use it
with the ndaal configuration it can be invoked as follows:

> ```shell
> gitleaks -c <path to ndaal gitleaks.toml> detect -v <path to repo to be scanned>
> ```

The above command displays the results to the stdout. If, however, the output
should be reported, this can be done giving the `-r` flag to give the path used
to create the report and the `-f` to give the format of the report. Here the
possibilities are:
  - json
  - csv
  - sarif

The default is a JSON report.

## List of Rules

The following list gives an overview over the type of credentials that can be
detected with the ndaal ruleset:

- Google storage API
- Google storage domain
- Appspot domain
- Cloudfront domain
- DigitalOcean domain
- AWS
- AWS MWS key
- AWS Manager ID
- AWS Secret Key
- AWS cred file info
- Adobe Client ID (Oauth Web)
- Adobe Client Secret
- Age secret key
- Alibaba AccessKey ID
- Alibaba Secret Key
- Amazon SNS Topic
- App SID for Twilio
- Asana Client ID
- Asana Client Secret
- Atlassian API token
- Beamer API token
- Bitbucket client ID
- Bitbucket client secret
- Bitcoin Address
- Bitly Secret Key
- Clojars API token
- Cloudinary Credentials
- Contentful delivery API token
- Credentials inside of a URI
- Databricks API token
- Discord API key
- Discord Webhook
- Discord client ID
- Discord client secret
- Doppler API token
- Dropbox API secret/key
- Dropbox long lived API token
- Dropbox short lived API token
- Duffel API token
- Dynatrace API token
- EasyPost API token
- EasyPost test API token
- Encrypted passwords in Linux files
- Facebook Access Token
- Facebook Client ID
- Facebook Secret Key
- Facebook token
- Fastly API token
- File name extensions of credentials
- File names of credentials
- Finicity API token
- Finicity client secret
- Flutterwave encrypted key
- Flutterwave public key
- Flutterwave secret key
- Frame.io API token
- Generic API Key
- Generic API Key on Windows
- Generic quoted credential
- Generic unquoted credential
- GitHub App Token
- GitHub OAuth Access Token
- GitHub Personal Access Token
- GitHub Refresh Token
- GitHub
- GitLab Personal Access Token
- GoCardless API token
- Google (GCP) Service-account
- Google API key
- Google Calendar URI
- Google Captcha
- Google Cloud Platform API Key
- Google FCM Server Key
- Google OAuth Access Token
- Google OAuth ID
- Grafana API token
- HTTP header that contains basic authorization
- HTTP header that contains bearer authorization
- Hardcoded credentials in Go files
- Hardcoded credentials in HCL files ({code}`*.tf`)
- Hardcoded credentials in JavaScript or TypeScript files
- Hardcoded credentials in PHP files
- Hardcoded login in JavaScript or TypeScript files
- Hardcoded secrets in Perl scripts
- Hardcoded secrets in Python scripts
- Hardcoded secrets in BASH script or setup
- HashiCorp Terraform user/org API token
- Heroku API Key
- High Entropy
- HubSpot API token
- Intercom API token
- Intercom client secret/ID
- Ionic API token
- JSON Web Token
- Linear API token
- Linear client secret/ID
- LinkedIn Client ID
- LinkedIn Client secret
- LinkedIn Secret Key
- Lob API Key
- Lob Publishable API Key
- MailChimp API key
- Mailgun API key
- Mailgun private API token
- Mailgun public validation key
- Mailgun webhook signing key
- Mapbox API token
- MessageBird API client ID
- MessageBird API token
- Microsoft Teams Webhook
- MongoDB Cloud Connection String
- New Relic Insight Key
- New Relic REST API Key
- New Relic Synthetic Locations Key
- New Relic admin API Key
- New Relic ingest browser API token
- New Relic user API ID
- New Relic user API Key
- NuGet API Key
- PGP private key
- PKCS8 private key
- PayPal
- PayPal Braintree access token
- Picatic API key
- PlanetScale API token
- PlanetScale password
- Postman API token
- Pulumi API token
- PyPI upload token
- RSA private key
- Riot Games Developer API Key
- Rubygem API token
- SSH (DSA) private key
- SSH (EC) private key
- SSH private key
- Secrets in JSON files
- Secrets in XML key of the key-value-pair
- Secrets in XML value of the key-value-pair
- SendGrid API token
- Sendinblue API token
- Serp API Key
- Shippo API token
- Shopify access token
- Shopify custom app access token
- Shopify private app access token
- Shopify shared secret
- Slack Webhook
- Slack token
- Slack v1 API Token
- Slack v2 API Token
- Square OAuth secret
- Square Personal Access Token
- Square access token
- StackHawk API Key
- Stripe
- Stripe API key
- Twilio API Key
- Twitch API token
- Twitter Client ID
- Twitter Secret Key
- Twitter token
- Typeform API token
- URLs of Amazaon AWS S3-Buckets
- US or Canada Zipcode
- WP-Config
- Zapier Webhook
- Zoho Webhook Token
- npm access token
- YAML secrets

## Sources for Ruleset

- <https://github.com/zricethezav/gitleaks/blob/master/config/gitleaks.toml>
- <https://github.com/Plazmaz/leaky-repo>
- <https://github.com/GSA/odp-code-repository-commit-rules/blob/master/gitleaks/rules.toml>
- <https://github.com/cloud-gov/caulking/blob/master/local.toml>
- <https://github.com/Typeform/gitleaks-config/blob/main/global_config.toml>
- <https://github.com/harry1080/secretx/blob/master/patterns.json>
- <https://github.com/hisxo/gitGraber/blob/master/tokens.py>
- <https://github.com/eth0izzle/shhgit/blob/3ce441853d999dacf6e20e59b116c135dcdd0c68/config.yaml>
- <https://github.com/w-digital-scanner/w13scan/blob/master/W13SCAN/scanners/PerFile/js_sensitive_content.py>
- <https://github.com/m4ll0k/SecretFinder/blob/master/BurpSuite-SecretFinder/SecretFinder.py>
- <https://github.com/projectdiscovery/nuclei-templates/blob/master/exposed-tokens/generic/credentials-disclosure.yaml>
- <https://github.com/udit-thakkur/AdvancedKeyHacks/blob/master/hackcura_apikey_hacks.sh>
- <https://github.com/gwen001/pentest-tools/blob/master/keyhacks.sh>
- <https://github.com/hahwul/dalfox/blob/1f32f3494e1aa3312f84b3e2a836eb61a9ae9aac/pkg/scanning/grep.go>

:::{Seealso}
The current version is available here [^footnote-4]
:::

______________________________________________________________________

```{rubric} Footnotes
```

[^footnote-1]: Mit Gitleaks Geheimnisse schützen und entdecken
    Protect and discover secrets using Gitleaks
    <https://github.com/zricethezav/gitleaks>

[^footnote-2]: GoLang
    <https://go.dev/>

[^footnote-3]: GitLeaks installation documentation
    <https://github.com/zricethezav/gitleaks#installing>

[^footnote-4]: ndaal - Modifikation zur Suche von Credentials mit Hilfe von GitLeaks
    ndaal - Secrets Search Based on GitLeaks
    <https://gitlab.com/ndaal_open_source/ndaal_secretes_search>

[^footnote-5]: ASP.NET Cryptography for Pentesters
    <https://blog.liquidsec.net/2021/06/01/asp-net-cryptography-for-pentesters/>